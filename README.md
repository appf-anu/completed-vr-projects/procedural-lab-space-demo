# ProceduralLabSpace

![VR app screenshot of a virtual lab space](Saved/AutoScreenshot.png)

Demonstration of a procedurally generated lab space fitting the room the VR device is configured for.

Developed with Unreal Engine 4.
